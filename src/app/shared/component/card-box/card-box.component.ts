import { Component, OnInit, Input } from '@angular/core';
import { Country } from '../../interface/country';

@Component({
  selector: 'app-card-box',
  templateUrl: './card-box.component.html',
  styleUrls: ['./card-box.component.scss']
})
export class CardBoxComponent implements OnInit {
  @Input() value: Country;
  constructor() { }

  ngOnInit(): void {
  }

}
