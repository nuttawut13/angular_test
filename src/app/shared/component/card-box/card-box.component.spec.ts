import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardBoxComponent } from './card-box.component';
import { SharedModule } from '../../shared.module';
import { CoreModule } from 'src/app/core/core.module';

describe('CardBoxComponent', () => {
  let component: CardBoxComponent;
  let fixture: ComponentFixture<CardBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardBoxComponent ],
      imports: [SharedModule, CoreModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
