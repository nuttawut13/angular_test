// angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// mat
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDividerModule } from '@angular/material/divider';

// component
import { DataNotFoundComponent } from './component/data-not-found/data-not-found.component';
import { CardBoxComponent } from './component/card-box/card-box.component';



@NgModule({
  declarations: [DataNotFoundComponent, CardBoxComponent],
  imports: [

    // Angular
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,

    // Material

    MatButtonModule,

    MatDividerModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule

  ],
  exports: [

    // angular
    FormsModule,

    // components
    CardBoxComponent,
    DataNotFoundComponent,


    // material
    MatButtonModule,
    MatDividerModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule

  ]
})
export class SharedModule { }
