import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { RootComponent } from './root/root.component';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    RootComponent
  ],
  imports: [
    AppRoutingModule,
    CoreModule,
    BrowserModule,
    // BrowserAnimationsModule,
    SharedModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
