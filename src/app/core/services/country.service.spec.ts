import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CountryService } from './country.service';

describe('CountryService', () => {
  let service: CountryService;
  let httpTest: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        CountryService
      ]
    });

    service = TestBed.inject(CountryService);
    httpTest = TestBed.get(HttpTestingController);
  });

  const dataMock = [
    {
      name: 'Thailand', capital: 'Bangkok', subregion: 'Southeast Asian', population: 67000000
    }
  ];

  const unformatData = [
    {
      "name": "Afghanistan", "topLevelDomain": [".af"], "alpha2Code": "AF", "alpha3Code": "AFG", "callingCodes": ["93"], "capital": "Kabul", "altSpellings": ["AF", "Afġānistān"], "region": "Asia", "subregion": "Southern Asia", "population": 27657145, "latlng": [33.0, 65.0], "demonym": "Afghan", "area": 652230.0, "gini": 27.8, "timezones": ["UTC+04:30"], "borders": ["IRN", "PAK", "TKM", "UZB", "TJK", "CHN"], "nativeName": "افغانستان", "numericCode": "004", "currencies": [{ "code": "AFN", "name": "Afghan afghani", "symbol": "؋" }], "languages": [{ "iso639_1": "ps", "iso639_2": "pus", "name": "Pashto", "nativeName": "پښتو" }, { "iso639_1": "uz", "iso639_2": "uzb", "name": "Uzbek", "nativeName": "Oʻzbek" }, { "iso639_1": "tk", "iso639_2": "tuk", "name": "Turkmen", "nativeName": "Türkmen" }], "translations": { "de": "Afghanistan", "es": "Afganistán", "fr": "Afghanistan", "ja": "アフガニスタン", "it": "Afghanistan", "br": "Afeganistão", "pt": "Afeganistão", "nl": "Afghanistan", "hr": "Afganistan", "fa": "افغانستان" }, "flag": "https://restcountries.eu/data/afg.svg", "regionalBlocs": [{ "acronym": "SAARC", "name": "South Asian Association for Regional Cooperation", "otherAcronyms": [], "otherNames": [] }], "cioc": "AFG"
    }
  ]

  afterEach(() => {
    httpTest.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be retrieved the data from api', () => {
    service.getCountryAPI().subscribe(res => {
      expect(res).toEqual(dataMock);
    })

    const req = httpTest.expectOne('https://restcountries.eu/rest/v2/all');
    expect(req.request.method).toBe('GET');
    req.flush(dataMock)

  });


});

