import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { Observable } from 'rxjs';
import { Country } from 'src/app/shared/interface/country';

@Injectable()
export class CountryService {
  private urlEnvironment = environment.url;
  private urlCountry = 'rest/v2/all';

  constructor(
    private http: HttpClient
  ) { }

  getCountryAPI(): Observable<Country[]> {
    return this.http.get<Country[]>(this.urlEnvironment + this.urlCountry).pipe(
      map(res => res.map(result => this.mappedItem(result)))); // Change data format only necessary one
  }

  private mappedItem(value: Country) {
    return {
      name: value.name,
      capital: value.capital,
      population: value.population,
      subregion: value.subregion
    }
  }

}
