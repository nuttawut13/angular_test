import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CountryService } from './services/country.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    CountryService
  ],
  exports: [
    HttpClientModule,
  ]
})
export class CoreModule { }
