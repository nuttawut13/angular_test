import { Component, OnInit } from '@angular/core';
import { Country } from '../shared/interface/country';
import { CountryService } from '../core/services/country.service';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit {


  data: Country[] = [];
  selectData = [
    {
      display: 'Country',
      value: 'name'
    },
    {
      display: 'Capital',
      value: 'capital'
    },
    {
      display: 'Subregion',
      value: 'subregion'
    },
    {
      display: 'Population',
      value: 'population'
    },
  ]
  searchFilter;
  searchBy = { display: 'Country', value: 'name' };


  constructor(
    private countryService: CountryService
  ) {

  }

  ngOnInit() {
    this.countryService.getCountryAPI().subscribe((res) => {
      this.data = res;
    })
  }


  /* FilterSearch method that allow user to search every field. 
  So value have to be converted in to string type and lowercase characters to be more accurate. 
  And if searchField is empty return original data. */
  filterSearch(searchValue: any, field) {
    if (searchValue) {
      return this.data.filter(res => {
        if (field) {
          return res[field].toString().toLowerCase().includes(searchValue.toLowerCase());
        }
      });
    } else {
      return this.data;
    }
  }

  compare(value1, value2): boolean {
    /* Compare func. First argument is from an option,
     second is from ngModel should be using this when value option is an object
    */

    return value1.display === value2.display
  }


}
